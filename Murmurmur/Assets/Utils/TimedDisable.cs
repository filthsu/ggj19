﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedDisable : MonoBehaviour {

    public float lifetime = 5;
    float t;

    private void OnEnable()
    {
        t = lifetime;
    }

    public void SetIme(float _t)
    {
        t = _t;
    }

    // Update is called once per frame
    void Update () {
        t -= Time.deltaTime;
        if(t <= 0)
        {
            gameObject.SetActive(false);
        }
	}
}
