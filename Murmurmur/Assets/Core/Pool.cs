﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour {

    public GameData.PrefabEntry info;

    Transform container;

    Queue<Poolable> pool;

    List<Poolable> objects;

    public void Initialize(GameData.PrefabEntry _info)
    {
        info = _info;
        container = new GameObject(info.name).transform;
        container.parent = transform;
        pool = new Queue<Poolable>();
        objects = new List<Poolable>();
        CreateObjects(info.poolSize);
    }

    public void Cleanup()
    {
        for(int i = 0; i < objects.Count; i++)
        {
            if (objects[i].gameObject.activeSelf)
            {
                objects[i].gameObject.SetActive(false);
            }
        }
    }

    void CreateObjects(int count)
    {
        for(int i = 0; i < count; i++)
        {
            GameObject go = Instantiate(info.prefab);
            Poolable p = go.AddComponent<Poolable>();
            go.transform.parent = container;
            p.owner = this;
            objects.Add(p);
            go.SetActive(false);
           // p.isAvailable = true;
           // pool.Enqueue(p);
        }
    }

    public void Push(Poolable p)
    {
        if (p.isAvailable) return;
        p.isAvailable = true;
        pool.Enqueue(p);
        p.transform.parent = container;
    }

    public GameObject Pop()
    {
        if(0 == pool.Count) { CreateObjects(1); }
        Poolable p = pool.Dequeue();
        p.isAvailable = false;
        return p.gameObject;
    }
}
