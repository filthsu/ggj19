﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSession : MonoBehaviour {

    public enum GameState
    {
        Initializing,
        Ready,
        Playing,
        Finished
    }

    public GameState state;

    [System.Serializable]
    public class Score
    {
        public int points;
        public float elapsedTime;
        public int bearsDrivenAway;
    }

    [DataPicker("Prefabs")]
    public int playerCharacter = 0;
    public FPSController player;

    [DataPicker("Prefabs")]
    public int trashbagPrefab = -1;

    public int trashbagsCount = 10;
    [HideInInspector]
    public List<TrashBag> trashBags;

    [DataPicker("Pool")]
    public int bearPrefab = -1;
    public float bearSpawnDistance = 50;
    public float waveDelay = 5;
    int waveNumber = 1;

    public Score score;

    /// <summary>
    /// Run behind loading screen
    /// </summary>
    public virtual IEnumerator Initialize()
    {
        state = GameState.Initializing;

        score = new Score();

        #region randomize trash bag positions & spawn them
        trashBags = new List<TrashBag>();
        List<TrashbagSpawnPoint> trashbagSpawns = new List<TrashbagSpawnPoint>(FindObjectsOfType<TrashbagSpawnPoint>());

        for (int i = 0; i < trashbagsCount; i++)
        {
            int index = Random.Range(0, trashbagSpawns.Count);
            TrashBag bag = Game.InstantiatePrefab(trashbagPrefab).GetComponent<TrashBag>();
            bag.transform.position = trashbagSpawns[index].transform.position;
            trashbagSpawns.RemoveAt(index);
            trashBags.Add(bag);

            if(i < trashbagsCount-1 && trashbagSpawns.Count == 0)
            {
                trashbagSpawns = new List<TrashbagSpawnPoint>(FindObjectsOfType<TrashbagSpawnPoint>());
            }
        }
        #endregion
        player = Game.InstantiatePrefab(playerCharacter).GetComponent<FPSController>();
        player.transform.position = Vector3.up;


        yield return null;
    }

    /// <summary>
    /// Starts running when loading screen is discarded
    /// </summary>
    public virtual IEnumerator Run()
    {
        UI.current.Cleanup();
        state = GameState.Ready;
        //Todo start game

        state = GameState.Playing;
        float waveTimer = waveDelay * .5f;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        for (; ; )
        {
            if (CheckEndcondition())
            {
                state = GameState.Finished;
            }
            if(state != GameState.Playing)
            {
                break;
            }
            if(bearCount <= 0 && !isSpawningWave)
            {
                waveTimer -= Time.deltaTime;
                if(waveTimer <= 0)
                {
                    waveTimer = waveDelay;
                    StartCoroutine(BearWave(waveNumber == 1 ? 1 : Mathf.Clamp(Random.Range(waveNumber / 3, waveNumber), 1, 10)));
                
                }
            }
            score.elapsedTime += Time.deltaTime;
            yield return null;
        }
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        state = GameState.Finished;

        UI.current.Push("GameOver");

        while(state == GameState.Finished)
        {
            yield return null;
        }
    }

    bool CheckEndcondition()
    {
        return (!player.isALive || 0 == trashBags.Count);
    }

    public int bearCount { get; set; }
    bool isSpawningWave = false;
    IEnumerator BearWave(int count = 1)
    {
        isSpawningWave = true;
        Debug.Log("Spawning " + count + " bears");
        while(count > 0)
        {
            Vector3 position = new Vector3(Random.value - .5f, 0, Random.value - .5f).normalized * bearSpawnDistance;
            position.y = 100;
            RaycastHit hit;
            if(!Physics.Raycast(position, Vector3.down, out hit, 1000 )) { yield return new WaitForSeconds(1f); continue; }

            Game.Pop(bearPrefab, hit.point, Quaternion.identity);

            count--;
            yield return new WaitForSeconds(2);
        }
        waveNumber++;
        isSpawningWave = false;
      
    }
}
