﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="GameData.asset", menuName = "Game Data File")]
public class GameData : ScriptableObject {

    public static GameData GetDataFile()
    {
        if (Application.isPlaying)
        {
            return Resources.Load<GameData>("GameData");
        }
        else
        {
#if UNITY_EDITOR
            return UnityEditor.AssetDatabase.LoadAssetAtPath<GameData>("Assets/Resources/GameData.asset");
#endif
        }
        return null;
    }

    [Scene]
    public string splash, menu, loading, game;

    [System.Serializable]
    public class SceneUIBinding
    {
        [Scene]
        public string scene;
        public UI ui;
    }

    public List<SceneUIBinding> sceneUIBindings;

    public List<GameSession> gameModes;

    [System.Serializable]
    public class PrefabEntry 
    {

        public string name;
        public int ID = -1;
        public int poolSize = 16;
        public GameObject prefab;
    }

    #region prefabs
    [System.Serializable]
    public class PrefabDatabase 
    {
        public List<PrefabEntry> entries;

        public GameObject GetPrefab(int id)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].ID == id) return entries[i].prefab;
            }
            return null;
        }

        public int Index2ID(int index)
        {
            return entries[index].ID;
        }

        public int ID2Index(int ID)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].ID == ID) return i;
            }
            return -1;
        }

        [System.NonSerialized]
        public int[] lookupTable;

        public void InitializeLookupTable()
        {
            int maxID = 0;
            for (int i = 0; i < entries.Count; i++)
            {
                maxID = Mathf.Max(maxID, entries[i].ID);
            }
            lookupTable = new int[maxID+1];
            for (int i = 0; i < maxID; i++)
            {
                lookupTable[i] = ID2Index(i);
            }
        }

        public string[] GetNames()
        {
            string[] result = new string[entries.Count + 1];
            result[0] = "None";
            for(int i = 0; i < entries.Count; i++)
            {
                result[i + 1] = entries[i].name;
            }
            return result;
        }

        public void RefreshIDs()
        {
            for (int i = entries.Count-1 ; i > -1; i--)
            {
                if (entries[i].ID < 0 || IsDuplicateID( entries[i].ID))
                {
                    entries[i].ID = NextFreeID();
                }
            }
        }

        bool IsDuplicateID( int id)
        {
            int count = 0;
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].ID == id)
                {
                    count++;
                    if (count > 1) return true;
                }
            }
            return false;
        }

        int NextFreeID()
        {
            int result = 0;
            for (int i = 0; i < entries.Count; i++)
            {
                if (i < 0) continue;
                if (entries[i].ID == result)
                {
                    result++;
                    i = -1;
                }
            }
            return result;
        }

    }
    [Space]
    [Header("Prefabs & pools")]
    public PrefabDatabase prefabs;
    public PrefabDatabase pools;

    void OnValidate()
    {
        ValidateDataIDs();
    }

    [EditorButton("Validate IDs")]
    public void ValidateDataIDs()
    {
        prefabs.RefreshIDs();
        pools.RefreshIDs();
        soundEffects.RefreshIDs();
    }

    #endregion

    #region Sounds
    [System.Serializable]
    public class SFX
    {
        public string name;
        public int ID = -1;
        public List<AudioClip> clips;
        [System.NonSerialized]
        public float lastPlayTime;
    }
    [Space]
    [Header("Audio Effects")]
    [DataPicker("Pool")]
    public int audioObject = -1;
    public SFXDatabase soundEffects;
    [System.Serializable]
    public class SFXDatabase
    {
        public List<SFX> entries;
        
        public bool CanPlay(int id)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].ID == id)
                {
                    bool result = entries[i].lastPlayTime < Time.time + .5f;
                    if (result) entries[i].lastPlayTime = Time.time;
                    return result;
                }
            }
            return false;
        }

        public AudioClip GetClip(int id)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].ID == id) return entries[i].clips[Random.Range(0, entries[i].clips.Count)];
            }
            return null;
        }

        public int Index2ID(int index)
        {
            return entries[index].ID;
        }

        public int ID2Index(int ID)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].ID == ID) return i;
            }
            return -1;
        }

        [System.NonSerialized]
        public int[] lookupTable;

        public void InitializeLookupTable()
        {
            int maxID = 0;
            for (int i = 0; i < entries.Count; i++)
            {
                maxID = Mathf.Max(maxID, entries[i].ID);
            }
            lookupTable = new int[maxID + 1];
            for (int i = 0; i < maxID; i++)
            {
                lookupTable[i] = ID2Index(i);
            }
        }

        public string[] GetNames()
        {
            string[] result = new string[entries.Count + 1];
            result[0] = "None";
            for (int i = 0; i < entries.Count; i++)
            {
                result[i + 1] = entries[i].name;
            }
            return result;
        }

        public void RefreshIDs()
        {
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].ID < 0 || IsDuplicateID(entries[i].ID))
                {
                    entries[i].ID = NextFreeID();
                }
            }
        }

        bool IsDuplicateID(int id)
        {
            int count = 0;
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].ID == id)
                {
                    count++;
                    if (count > 1) return true;
                }
            }
            return false;
        }

        int NextFreeID()
        {
            int result = 0;
            for (int i = 0; i < entries.Count; i++)
            {
                if (i < 0) continue;
                if (entries[i].ID == result)
                {
                    result++;
                    i = -1;
                }
            }
            return result;
        }
    }


    #endregion

    #region Music
    public List<AudioClip> musicTracks;
    #endregion
}
