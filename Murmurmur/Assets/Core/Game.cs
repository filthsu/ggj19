﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour {

    [System.Serializable]
    public class PlayerPreferences
    {
        public float mouseSensitivity = 200;
        public bool invertMouse = false;

        public float audioVolume = 1;
        public float musicVolume = 1;
    }

    public class SessionData
    {
        public bool dirty { get; set; }
        public GameSession sessionPrefab;
        public GameSession currentSession;
    }

    public static Game instance { get; private set; }
    public static GameData data { get; private set; }
    public static SessionData sessionData { get; private set; }
    public static PlayerPreferences preferences;

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
        data = Instantiate(GameData.GetDataFile());
      
        SceneManager.LoadScene(data.splash);
        SceneManager.sceneLoaded += this.OnSceneLoaded;

        LoadPreferences();

        gameObject.AddComponent<Screenshot>();
    }

    const string preferencesPath = "PLAYER_PREFERENCES";
    public void LoadPreferences()
    {
        string preferencesJson = PlayerPrefs.GetString(preferencesPath);
        if (string.IsNullOrEmpty(preferencesJson))
        {
            preferences = new PlayerPreferences();
        }
        else
        {
            try
            {
                preferences = JsonUtility.FromJson<PlayerPreferences>(preferencesJson);
            }
            catch
            {
                preferences = new PlayerPreferences();
            }
        }
    }

    public void SavePreferences()
    {
        string json = JsonUtility.ToJson(preferences);
        PlayerPrefs.SetString(preferencesPath, json);
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if(scene.name == data.loading)
        {
            if (UI.current)
            {
                Destroy(UI.current.gameObject);
            }
        }

        for(int i = 0; i < data.sceneUIBindings.Count; i++)
        {
            if(data.sceneUIBindings[i].scene == scene.name)
            {
                Instantiate(data.sceneUIBindings[i].ui);
            }
        }
    }

    private void Start()
    {
        StartCoroutine(RunSession());
        StartCoroutine(PlayMusic());
    }

    private void Update()
    {
        if (musicSource)
        {
            musicSource.volume = preferences.musicVolume;
        }
    }

    IEnumerator RunSession()
    {
        sessionData = new SessionData();
        //Init & load menu
        InitPoolsAndPrefabs();
        yield return SceneManager.LoadSceneAsync(data.menu);
        
        for (; ; )
        {
            if (sessionData.dirty)
            {
                for (int i = 0; i < pools.Length; i++)
                {
                    pools[i].Cleanup();
                }
                if (null != sessionData.sessionPrefab)
                {
                    //Game start requested
                    yield return SceneManager.LoadSceneAsync(data.loading);
                    yield return SceneManager.LoadSceneAsync(data.game, LoadSceneMode.Additive);
                    SceneManager.SetActiveScene(SceneManager.GetSceneByName(data.game));
                    sessionData.currentSession = Instantiate<GameSession>(sessionData.sessionPrefab);
                    yield return sessionData.currentSession.Initialize();
                    yield return SceneManager.UnloadSceneAsync(data.loading);
                    sessionData.currentSession.StartCoroutine(sessionData.currentSession.Run());
                }
                else
                {
                    //Game end requested
                    if (null != sessionData.currentSession)
                    {
                        Destroy(sessionData.currentSession.gameObject);
                        yield return SceneManager.LoadSceneAsync(data.menu);
                    }
                }
                sessionData.dirty = false;
            }
            yield return null;
        }
    }

    public void SetCommand(string command)
    {
        string[] cmd = command.Split(' ');
        switch (cmd[0])
        {
            case "startgame":
                int gameModeIndex = 0;
                if(cmd.Length > 1)
                {
                    int.TryParse(cmd[1], out gameModeIndex);
                }
                sessionData.dirty = true;
                sessionData.sessionPrefab = data.gameModes[gameModeIndex];
                break;
            case "restartgame":

                sessionData.dirty = true;
                break;
            case "exittomenu":
                sessionData.sessionPrefab = null;
                sessionData.dirty = true;
                break;
            case "exitgame":
                Application.Quit();
                break;
            case "save_prefs":
                SavePreferences();
                break;
        }
    }

    /// <summary>
    /// Instantiate prefab from data
    /// </summary>
    public static GameObject InstantiatePrefab(int ID)
    {
        return GameObject.Instantiate(data.prefabs.GetPrefab(ID));
    }

    void InitPoolsAndPrefabs()
    {
        data.prefabs.InitializeLookupTable();
        data.pools.InitializeLookupTable();
        pools = new Pool[data.pools.lookupTable.Length];
        for(int i = 0; i < data.pools.entries.Count; i++)
        {
            Pool pool = gameObject.AddComponent<Pool>();
            pool.Initialize(data.pools.entries[i]);
            pools[data.pools.entries[i].ID] = pool;
        }
    }
    Pool[] pools;

    /// <summary>
    /// Pop from pool data
    /// </summary>
    public static GameObject Pop(int ID, Vector3 position, Quaternion rotation)
    {
        GameObject go = instance.pools[ID].Pop();
        go.transform.position = position;
        go.transform.rotation = rotation;
        go.SetActive(true);
        return go;
    }

    /// <summary>
    /// Play sound effect
    /// </summary>
    public static void PlaySound(int ID, Vector3 position, float volume, float spatial)
    {
        if (ID < 0) return;
        if (!data.soundEffects.CanPlay(ID)) return;
        GameObject go = Pop(data.audioObject, position, Quaternion.identity);
        AudioClip clip = data.soundEffects.GetClip(ID);
        AudioSource audio = go.GetComponent<AudioSource>();
        audio.volume = volume * preferences.audioVolume;
        audio.spatialBlend = spatial;
        audio.PlayOneShot(clip);
        go.GetComponent<TimedDisable>().SetIme(clip.length + .1f);
    }

    public AudioSource musicSource;

    IEnumerator PlayMusic()
    {
        musicSource = gameObject.AddComponent<AudioSource>();
        musicSource.spatialBlend = 0;
        musicSource.playOnAwake = false;
        musicSource.loop = false;
        while (null == data) yield return null;
        if (null == data.musicTracks || 0 == data.musicTracks.Count) yield break;
        int currentTrack = Random.Range(0, data.musicTracks.Count);
        for (; ; )
        {
            musicSource.clip = data.musicTracks[currentTrack];
            musicSource.Play();
            while (musicSource.isPlaying)
            {
                yield return null;
            }
            int nextTrack = currentTrack;
            if(data.musicTracks.Count > 1)
            {
                while(nextTrack == currentTrack)
                {
                    nextTrack = Random.Range(0, data.musicTracks.Count);
                    yield return null;
                }
            }
            currentTrack = nextTrack;
            yield return null;
        }
    }
}
