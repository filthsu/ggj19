﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poolable : MonoBehaviour {
    [HideInInspector]
    public Pool owner;

    public bool isAvailable { get; set; }

    private void OnDisable()
    {
        owner.Push(this);
    }
}
