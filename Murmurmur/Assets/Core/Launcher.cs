﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Launcher
{
    [RuntimeInitializeOnLoadMethod]
    static void OnGameLaunch()
    {
        Debug.Log("Launcher::OnGameLaunch");
        Game game = new GameObject("Game").AddComponent<Game>();
    }
}
