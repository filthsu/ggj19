﻿using System;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Reflection;

/// <summary>
/// Stick this on a method
/// </summary>
[System.AttributeUsage(System.AttributeTargets.Method)]
public class EditorButtonAttribute : PropertyAttribute
{
    public string label;
    public EditorButtonAttribute(string buttonLabel)
    {
        this.label = buttonLabel;
    }
}

#if UNITY_EDITOR

[CanEditMultipleObjects]
[CustomEditor(typeof(MonoBehaviour), true)]
public class EditorButton : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var mono = target as MonoBehaviour;

        var methods = mono.GetType()
            .GetMembers(BindingFlags.Instance | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public |
                        BindingFlags.NonPublic)
            .Where(o => Attribute.IsDefined(o, typeof(EditorButtonAttribute)));

        foreach (var memberInfo in methods)
        {
            object[] attrs = memberInfo.GetCustomAttributes(false);
            for (int i = 0; i < attrs.Length; i++)
            {
                if (attrs[i] is EditorButtonAttribute)
                {
                    
                    if (GUILayout.Button((attrs[i] as EditorButtonAttribute).label))
                    {
                        var method = memberInfo as MethodInfo;
                        method.Invoke(mono, null);
                    }
                    //return;
                }
            }
        }
    }
}

#endif