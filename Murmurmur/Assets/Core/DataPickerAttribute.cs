﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class DataPickerAttribute : PropertyAttribute {
    public string databaseName;

    public DataPickerAttribute(string dbName)
    {
        databaseName = dbName;
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(DataPickerAttribute))]
public class DataPickerDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        DataPickerAttribute prop = attribute as DataPickerAttribute;

        EditorGUI.BeginProperty(position, label, property);
        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        if (property.propertyType == SerializedPropertyType.Integer)
        {
            GameData data = GameData.GetDataFile();
            
            GameData.PrefabDatabase db = null;
            GameData.SFXDatabase sfxDB = null;

            if(null == data.prefabs) { data.prefabs = new GameData.PrefabDatabase(); }
            if(null == data.pools) { data.pools = new GameData.PrefabDatabase(); }

            switch (prop.databaseName)
            {
                case "Prefab":
                default:
                    db = data.prefabs;
                    break;
                case "Pool":
                    db = data.pools;
                    break;
                case "SFX":
                    db = null;
                    sfxDB = data.soundEffects;
                    break;
            }

            if (null != db)
            {
                string[] names = db.GetNames();
                if (null != names)
                {
                    int id = property.intValue;
                    int i = db.ID2Index(id);
                    i = EditorGUI.Popup(position, i + 1, names) - 1;
                    try
                    {
                        id = db.Index2ID(i);
                    }
                    catch
                    {
                        id = -1;
                    }
                    property.intValue = id;
                }
                else
                {
                    EditorGUI.LabelField(position, "Database is empty or corrupted."); 
                }
            }
            else if(null != sfxDB)
            {
                string[] names = sfxDB.GetNames();
                if (null != names)
                {
                    int id = property.intValue;
                    int i = sfxDB.ID2Index(id);
                    i = EditorGUI.Popup(position, i + 1, names) - 1;
                    try
                    {
                        id = sfxDB.Index2ID(i);
                    }
                    catch
                    {
                        id = -1;
                    }
                    property.intValue = id;
                }
                else
                {
                    EditorGUI.LabelField(position, "Database is empty or corrupted.");
                }
            }
        }
        else
        {
            EditorGUI.LabelField(position, "Error: not an integer value");
        }
        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();

    }
    
}
#endif
