﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour {

    public static UI current { get; private set; }
    public UIPage landingPage;
    UIPage currentPage;
    UIPage[] pages;
    Stack<UIPage> history;

    protected virtual void Awake()
    {
        if (current)
        {
            Destroy(current.gameObject);
        }
        current = this;
        DontDestroyOnLoad(gameObject);
        pages = GetComponentsInChildren<UIPage>(true);
        history = new Stack<UIPage>();
        for(int i = 0; i < pages.Length; i++)
        {
            pages[i].gameObject.SetActive(false);
        }
        if(null == landingPage)
        {
            landingPage = pages[0];
        }
    }
    protected virtual void Start()
    {
        Push(landingPage);
    }

    public void Cleanup()
    {
        history.Clear();
    }

    public void Push(UIPage page)
    {
        if(null != currentPage)
        {
            history.Push(currentPage);
            currentPage.gameObject.SetActive(false);
        }
        currentPage = page;
        currentPage.gameObject.SetActive(true);
    }

    public void Push(string name)
    {
        for(int i = 0; i < pages.Length; i++)
        {
            if(pages[i].name == name)
            {
                Push(pages[i]);
                return;
            }
        }
    }

    public void Pop()
    {
        if(history.Count > 0)
        {
            if (currentPage) { currentPage.gameObject.SetActive(false); }
            currentPage = history.Pop();
            currentPage.gameObject.SetActive(true);
        }
    }

    public void SendCommandtoGame(string command)
    {
        Game.instance.SetCommand(command);
    }
}
