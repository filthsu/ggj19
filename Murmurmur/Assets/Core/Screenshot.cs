﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class Screenshot : MonoBehaviour {

    public int scale = 1;
    public KeyCode screenshotKey = KeyCode.F10;

    private void Update()
    {
        if (Input.GetKeyDown(screenshotKey))
        {
            CaptureScreen();
        }
    }

    [EditorButton("Take screenshot")]
    public void CaptureScreen()
    {
        string path = Application.dataPath.Remove(Application.dataPath.Length - ("Assets".Length));
        if (!Directory.Exists(path + "Screenshots"))
        {
            Directory.CreateDirectory(path + "Screenshots");
        }
        path += "Screenshots";
        System.DateTime now = System.DateTime.Now;
        string filename = string.Format("{0}{1}{2}{3}", now.Year, now.Month, now.Day, now.Ticks);
        ScreenCapture.CaptureScreenshot(path + "/" + filename + ".png", scale);
    }
    [EditorButton("Open screenshots folder")]
    public void OpenScreenshotsFolder()
    {
        string path = Application.dataPath.Remove(Application.dataPath.Length - ("Assets".Length));
        if (!Directory.Exists(path + "Screenshots"))
        {
            Directory.CreateDirectory(path + "Screenshots");
        }
        path += "Screenshots";
        System.Diagnostics.Process.Start("explorer.exe", path.Replace('/', '\\'));
    }
}
