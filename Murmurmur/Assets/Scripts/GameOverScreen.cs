﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour {

    public Text pointsView;
    public GameObject buttonsRoot;
    private void OnEnable()
    {
        StartCoroutine(PostgameRoutine());   
    }

    IEnumerator PostgameRoutine()
    {
        pointsView.text = "";
        buttonsRoot.SetActive(false);
        yield return new WaitForSeconds(1);
        if (Game.sessionData.currentSession.score.points > 0)
        {
            float num = 0;
            float t = 2;
            while (num < Game.sessionData.currentSession.score.points)
            {
                num = Mathf.Lerp(num, Game.sessionData.currentSession.score.points, Time.deltaTime * 20);
                pointsView.text = (Mathf.CeilToInt(num)).ToString();
                t -= Time.deltaTime;
                if (t < 0)
                {
                    buttonsRoot.SetActive(true);
                }
                yield return null;
            }
        }
        buttonsRoot.SetActive(true);
        yield return null;
    }
}
