﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearCountry : MonoBehaviour {

    public float radius = 50;
    [DataPicker("SFX")]
    public int killSound = -1;
    float t = 0;
    private void Update()
    {
        if (Game.sessionData.currentSession)
        {
            if (Game.sessionData.currentSession.player)
            {
                if (Game.sessionData.currentSession.player.isALive)
                {
                    if (Game.sessionData.currentSession.player.transform.position.Flatten().magnitude > radius)
                    {
                        t += Time.deltaTime;
                        if (t > 1)
                        {

                            Game.PlaySound(killSound, Game.sessionData.currentSession.player.cameraTransform.position - Game.sessionData.currentSession.player.cameraTransform.forward, 2, 1);
                            Game.sessionData.currentSession.player.InstaKill(Vector3.up + (-Game.sessionData.currentSession.player.transform.position.normalized));
                        }

                    }
                    else
                    {
                        t = 0;
                    }
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        for(int i = 0; i < 360; i++)
        {
            Vector3 p = new Vector3(Mathf.Cos(i * Mathf.Deg2Rad), 0, Mathf.Sin(i * Mathf.Deg2Rad)).normalized * radius;

            Gizmos.color = Color.red;
            Gizmos.DrawSphere(p, .2f);
        }
    }
}
