﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHitEventReceiver
{
    void OnHit();
}

public class TrashBag : MonoBehaviour, IHitEventReceiver
{
    public float health = 1.0f;
    public Material[] materials;

    [DataPicker("Pool")]
    public int onHitParticleEffect = -1;

    private void OnEnable()
    {
        GetComponentInChildren<MeshRenderer>().sharedMaterial = materials[Random.Range(0, materials.Length)];
    }

    public void ReceiveBearAttack()
    {
        if (health > 0)
        {
            OnHit();
            health -= .1f;
            if (health <= 0)
            {
                Game.sessionData.currentSession.trashBags.Remove(this);
                Destroy(gameObject);
                //TODO: Visual feedback

                if (Random.value < .25f)
                {
                    Game.PlaySound(Game.sessionData.currentSession.player.perkeleSound, Game.sessionData.currentSession.player.transform.position, 1, 1);
                }

                Debug.Log("Trash bag lost!");
            }
        }
    }

    public void OnHit()
    {
        if(onHitParticleEffect > -1)
        {
            Game.Pop(onHitParticleEffect, transform.position, Quaternion.identity);
        }
    }
}
