﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Bear : MonoBehaviour {

    //The position bear was spawned for this wave. Used to remember where to go when returning to the woods
    public Vector3 rootPoint;
    public Vector3 targetPoint;

    public float health = 1.0f;
    public float trashAttackCooldown = 2f;
    public float playerAttackRange = 3;

    float currentSpeed = 0;
    public float walkSpeed = 5;
    public float escapeSpeed = 15;

    NavMeshAgent navMeshAgent;
    NavMeshPath currentPath;

    public Vector3 pushForce = Vector3.zero;

    [DataPicker("Pool")] public int damageVFX = -1;
    #region Animations
    Animator animator;


    #endregion

    bool isActive;
    float hitReceiveCooldown = 0;
    public void ReceiveShove()
    {
        if (isActive && hitReceiveCooldown <= 0)
        {
            hitReceiveCooldown = .25f;
            Game.sessionData.currentSession.score.points += 100;
            currentSpeed = 0;
            navMeshAgent.speed = 0;
            health -= .25f;
            Game.Pop(damageVFX, Game.sessionData.currentSession.player.cameraTransform.transform.TransformPoint(Vector3.forward * 1.5f), Quaternion.identity);
            if (health <= 0)
            {
                isActive = false;
                Game.sessionData.currentSession.score.bearsDrivenAway++;
                Game.PlaySound(this.escapeSound, transform.position, 1, 1);
            }
        }
    }

    private void Update()
    {
        navMeshAgent.speed = currentSpeed;
        if(hitReceiveCooldown > 0)
        {
            hitReceiveCooldown -= Time.deltaTime;
        }

        transform.position += pushForce.Flatten() * Time.deltaTime;
        pushForce = Vector3.Lerp(pushForce, Vector3.zero, Time.deltaTime);

        tearsParticles.gameObject.SetActive(health <= 0);
    }

    private void OnEnable()
    {
        animator = GetComponentInChildren<Animator>();
        if (Game.sessionData != null && Game.sessionData.currentSession)
        {
            isActive = true;
            health = 1;
            navMeshAgent = GetComponent<NavMeshAgent>();
            currentPath = new NavMeshPath();
            navMeshAgent.ResetPath();
            navMeshAgent.nextPosition = transform.position;
            rootPoint = transform.position;
            Game.sessionData.currentSession.bearCount++;
            navMeshAgent.updatePosition = true;
            StartCoroutine(UpdateAI());
        }
    }

    private void OnDisable()
    {
        if (Game.sessionData != null && Game.sessionData.currentSession)
        {
            Game.sessionData.currentSession.bearCount--;
        }
    }

    IEnumerator CalculatePath()
    {
        int attempts = 32;
        while(!NavMesh.CalculatePath(transform.position, targetPoint, int.MaxValue, currentPath))
        {
            attempts--;
            if(attempts <= 0)
            {
                if (health <= 0) { break; }
                if (Game.sessionData.currentSession.trashBags.Count > 0)
                {
                    targetBag = Game.sessionData.currentSession.trashBags[Random.Range(0, Game.sessionData.currentSession.trashBags.Count)];
                }
                attempts = 32;
            }
            yield return new WaitForSeconds(1f);
            yield return null;
        }
        if (currentPath != null)
        {
            navMeshAgent.SetPath(currentPath);
        }
    }

    [DataPicker("SFX")] public int idleSound = -1;
    [DataPicker("SFX")] public int escapeSound = -1;

    TrashBag targetBag;
    IEnumerator UpdateAI()
    {
        animator.Play("Walk");
        //Pick target bag
        currentSpeed = walkSpeed;

        targetBag = Game.sessionData.currentSession.trashBags[Random.Range(0, Game.sessionData.currentSession.trashBags.Count)];
        targetPoint = targetBag.transform.position;
        yield return CalculatePath();

        for(; ; )
        {
            currentSpeed = Mathf.Lerp(currentSpeed, walkSpeed, Time.deltaTime);
            if(health <= 0) { break; }
            if (0 == Game.sessionData.currentSession.trashBags.Count) { break; }

            if (null == targetBag)
            {
                while(null == targetBag)
                {
                    if (health <= 0) { break; }
                    if(0 == Game.sessionData.currentSession.trashBags.Count) { break; }
                    targetBag = Game.sessionData.currentSession.trashBags[Random.Range(0, Game.sessionData.currentSession.trashBags.Count)];
                    yield return null;
                }
                if (targetBag)
                {
                    targetPoint = targetBag.transform.position;
                    yield return CalculatePath();
                }
                else
                {
                    break;
                }
            }

            //Kill player
            if(Vector3.Distance(Game.sessionData.currentSession.player.transform.position, transform.position) < playerAttackRange && Game.sessionData.currentSession.player.isALive)
            {
                Game.PlaySound(this.idleSound, transform.position, 1, 1);
                if(Random.value < .45f)
                {
                    Game.PlaySound(Game.sessionData.currentSession.player.perkeleSound, Game.sessionData.currentSession.player.transform.position, 1, 1);
                }
                float attackTime = 0;
                animator.Play("AttackStart");
                while(Vector3.Distance(Game.sessionData.currentSession.player.transform.position, transform.position) < playerAttackRange)
                {
                    if (health <= 0) { break; }
                    navMeshAgent.transform.rotation = Quaternion.Slerp(navMeshAgent.transform.rotation, Quaternion.LookRotation((Game.sessionData.currentSession.player.transform.position - transform.position).Flatten(), Vector3.up * .1f), Time.deltaTime * 90);
                    currentSpeed = 0;
                    attackTime += Time.deltaTime;
                    if(attackTime > 2f)
                    {
                        //Kill the player.
                        animator.Play("AttackExecute");
                        Game.sessionData.currentSession.player.InstaKill(-transform.right*2 + Vector3.up);
                        Game.PlaySound(this.idleSound, transform.position, 1, 1);
                        Game.PlaySound(Game.sessionData.currentSession.player.perkeleSound, Game.sessionData.currentSession.player.transform.position, 1, 1);
                        break;
                    }
                    yield return null;
                }
                currentSpeed = walkSpeed;
            }

            if (!targetBag)
            {
                break;
            }

            if(Vector3.Distance(transform.position, targetBag.transform.position) < playerAttackRange)
            {
                //Kill the bag
                animator.Play("DumpsterDive");
                currentSpeed = 0;
                while(targetBag && targetBag.health > 0)
                {
                    if (health <= 0) { break; }
                    if (Vector3.Distance(transform.position, targetBag.transform.position) > playerAttackRange) break;
                    float t = trashAttackCooldown;
                    if(Random.value < .25f)
                    {
                        Game.PlaySound(this.idleSound, transform.position, 1, 1);
                    }
                    while(t > 0)
                    {
                        if (health <= 0) { break; }
                        if (!targetBag) break;
                        if (targetBag.health <= 0) break;
                        if (Vector3.Distance(transform.position, targetBag.transform.position) > playerAttackRange) break;
                        t -= Time.deltaTime;
                        if (t <= 0)
                        {
                            targetBag.ReceiveBearAttack();
                            break;
                        }
                        yield return null;
                    }
                    yield return null;
                }

                //Pick new target
                while (null == targetBag)
                {
                    if (health <= 0) { break; }
                    if (Game.sessionData.currentSession.trashBags.Count > 0)
                    {
                        targetBag = Game.sessionData.currentSession.trashBags[Random.Range(0, Game.sessionData.currentSession.trashBags.Count)];
                        if (targetBag)
                        {
                            if (Vector3.Distance(transform.position, targetBag.transform.position) > playerAttackRange) break;
                        }
                    }
                    else
                    {
                        break;
                    }
                    yield return null;
                }
                if (targetBag)
                {
                    targetPoint = targetBag.transform.position;
                    currentSpeed = walkSpeed;
                }
                else
                {
                    break;
                }
                yield return CalculatePath();
            }
            
            animator.Play("Walk");
            yield return null;
        }
        animator.Play("RunAway");
        
        //Health depleted, escape back to the woods
        targetPoint = rootPoint;
        yield return CalculatePath();
        currentSpeed = escapeSpeed;

        float autoDestructTimer = 10f;
        while(Vector3.Distance(transform.position, rootPoint) > 1)
        {
            autoDestructTimer -= Time.deltaTime;
            if (autoDestructTimer <= 0) break;
            if (Random.value < .02f)
            {
                Game.PlaySound(this.escapeSound, transform.position, 1, 1);
            }

            yield return CalculatePath();
            yield return null;
        }
        gameObject.SetActive(false);

        
    }

    public GameObject tearsParticles;
    

    private void OnCollisionEnter(Collision collision)
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
}
