﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashbagSpawnPoint : MonoBehaviour {

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0, 0, 0, .5f);
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawSphere(Vector3.zero, .25f);
        Gizmos.DrawWireSphere(Vector3.zero, .25f);
    }
}
