﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour {
    public Text scoreView;
    float scoreViewValue;
	// Use this for initialization
	void Start () {
        scoreViewValue = 0;
        scoreView.text = "0";
	}
	
	// Update is called once per frame
	void Update () {
        scoreViewValue = Mathf.Lerp(scoreViewValue, Game.sessionData.currentSession.score.points, Time.deltaTime * 20);
        scoreView.text = (Mathf.CeilToInt(scoreViewValue)).ToString();
	}
}
