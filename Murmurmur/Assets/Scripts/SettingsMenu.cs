﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour {

    public Slider sfxVolume, musicVolume, mouseSensitivity;
    public Toggle invertMouse;

    private void OnEnable()
    {
        if(null != Game.preferences)
        {
            sfxVolume.value = Game.preferences.audioVolume;
            musicVolume.value = Game.preferences.musicVolume;
            mouseSensitivity.value = Game.preferences.mouseSensitivity;
            invertMouse.isOn = Game.preferences.invertMouse;
        }
    }

    public void OnSFXVolumeChanged(float f)
    {
        Game.preferences.audioVolume = f;
    }

    public void OnMusicVolumeChanged(float f)
    {
        Game.preferences.musicVolume = f;
    }

    public void OnMouseSensitivityChanged(float f)
    {
        Game.preferences.mouseSensitivity = f;
    }

    public void OnOnvertMouseChanged(bool b)
    {
        Game.preferences.invertMouse = b;
    }

    public void Close()
    {
        Game.instance.SavePreferences();
        UI.current.Pop();
    }
}
