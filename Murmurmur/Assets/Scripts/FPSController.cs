﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class FPSController : MonoBehaviour {

    public bool isALive { get; private set; }



	// Use this for initialization
	void Start () {
        isALive = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (isALive && Game.sessionData.currentSession.state != GameSession.GameState.Finished)
        {
            UpdateMouseLook();
            UpdateMovement();
            if (Input.GetMouseButton(0))
            {
                Shove();
            }
        }
	}

    #region Mouse look
    public Transform cameraTransform;
    Vector3 cameraRotation;
    Vector3 viewRotation;
    public Vector2 verticalLimits = new Vector2(-80, 80);
    public string horizontalLookAxis = "Mouse X";
    public string verticalLookAxis = "Mouse Y";
    void UpdateMouseLook()
    {
        viewRotation.y = Mathf.Repeat(viewRotation.y + Game.preferences.mouseSensitivity * Time.deltaTime * Input.GetAxis(horizontalLookAxis), 360f);
        cameraRotation.x = Mathf.Clamp(cameraRotation.x - Game.preferences.mouseSensitivity * (Game.preferences.invertMouse ? -1 : 1) * Time.deltaTime * Input.GetAxis(verticalLookAxis), verticalLimits.x, verticalLimits.y);
        cameraTransform.localRotation = Quaternion.Euler(cameraRotation);
        transform.localRotation = Quaternion.Euler(viewRotation);
    }
    #endregion

    #region Movement
    public CharacterController characterController;
    public float moveSpeed = 15;
    public string horizontalMoveAxis = "Horizontal";
    public string verticalMoveAxis = "Vertical";
    public KeyCode jumpKey = KeyCode.Space;
    Vector3 motionVector;
    public float gravityScale;
    public float jumpScale;
    void UpdateMovement()
    {
        
        Vector3 motion = new Vector3(Input.GetAxis(horizontalMoveAxis),0, Input.GetAxis(verticalMoveAxis));
        if(motion.sqrMagnitude > 1) { motion = motion.normalized; }
        motion = transform.TransformDirection(motion) * moveSpeed;

        if (characterController.isGrounded)
        {
            motionVector.x = motion.x;
            motionVector.z = motion.z;
           // motionVector.y = 0;
            if (Input.GetKeyDown(jumpKey))
            {
                motionVector.y = jumpScale;
            }
        }
        else
        {
            motionVector.x = Mathf.Lerp(motionVector.x, 0, Time.deltaTime);
            motionVector.z = Mathf.Lerp(motionVector.z, 0, Time.deltaTime);
            motionVector.y -= gravityScale * Time.deltaTime;
        }

        characterController.Move(motionVector * Time.deltaTime);
    }
    #endregion

    #region Broom
    [DataPicker("SFX")]
    public int broomSound = -1;
    bool canShove = true;
    public float shoveCooldown = .75f;
    public Transform broomTransform;
    public float shoveRange = 2;
    public float shoveRadius = .25f;
    public LayerMask shoveAffectedLayers;
    void Shove()
    {
        if (canShove)
        {
            canShove = false;
            Game.PlaySound(broomSound, cameraTransform.position, 1, 1);
            StartCoroutine(ShoveAnimation());
            Collider[] result = Physics.OverlapCapsule(broomTransform.position, broomTransform.position + cameraTransform.forward * shoveRange, shoveRadius, shoveAffectedLayers);
            for(int i = 0; i < result.Length; i++)
            {
                Bear bear = result[i].GetComponentInParent<Bear>();
                if (bear)
                {
                    bear.ReceiveShove();
                    bear.pushForce += cameraTransform.forward.Flatten() * 10;
                }
                else
                {
                    Rigidbody rb = result[i].GetComponentInParent<Rigidbody>();
                    if (rb)
                    {
                        rb.AddForce(cameraTransform.forward.Flatten() * 10 + Vector3.up, ForceMode.Impulse);
                    }
                }

                IHitEventReceiver hitReceiver = result[i].GetComponentInParent<IHitEventReceiver>();
                if(null != hitReceiver)
                {
                    hitReceiver.OnHit();
                }
            }
        }
    }

  

    IEnumerator ShoveAnimation()
    {
        float t = 0;
        while(t < .95f)
        {
            t = Mathf.Lerp(t, 1, Time.deltaTime * 20);
            broomTransform.localPosition = Vector3.Lerp(Vector3.zero, Vector3.forward*1, t);
            yield return null;
        }
        while(t > .05f)
        {
            if (!isALive) yield break;
            t = Mathf.Lerp(t, 0, Time.deltaTime * 8);
            broomTransform.localPosition = Vector3.Lerp(Vector3.zero, Vector3.forward, t);
            yield return null;
        }
        broomTransform.localPosition = Vector3.zero;
        yield return new WaitForSeconds(shoveCooldown);
        canShove = true;
    }
    #endregion

    #region Scream
    [DataPicker("SFX")] public int perkeleSound = -1;
    void UpdateMicrophoneInput()
    {
     
    }
    #endregion

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Bear bear = hit.collider.GetComponent<Bear>();
        if (bear)
        {
            return;
        }
        Rigidbody rb = hit.gameObject.GetComponent<Rigidbody>();
        if (rb)
        {
            rb.AddForce(motionVector + Vector3.up, ForceMode.Impulse);
        }

    }

    public GameObject beheadedBlood;

    public void InstaKill(Vector3 hitDirection)
    {
        if (!isALive) return;
        isALive = false;
        Rigidbody rb = cameraTransform.gameObject.AddComponent<Rigidbody>();
        rb.angularDrag = 1;
        rb.drag = 1;

        cameraTransform.gameObject.AddComponent<SphereCollider>().radius = .2f;
        Destroy(broomTransform.gameObject);
        rb.AddForce(hitDirection * 10, ForceMode.Impulse);

        beheadedBlood.gameObject.SetActive(true);

    }
}
